package gormdb

// # Database config
// db.driver= 			# default=sqlite3
// db.host= 			# default=localhost (which resolves to /tmp/app.db when db.driver is sqlite3)
// db.port= 			# default=""
// db.user= 			# default=default
// db.name= 			# default=default
// db.password= 		# default=""
// db.singulartable= 	# default=false
// db.log= 				# default=false
// db.autoinit=  		# default=true

import (
	"context"
	"fmt"
	"time"

	"github.com/revel/revel"
	"github.com/revel/revel/logger"

	"gorm.io/driver/mysql"
	"gorm.io/driver/postgres"
	"gorm.io/gorm"
	glog "gorm.io/gorm/logger"
	// mysql package
	// postgres package
	// mysql package
)

var (
	// DB Gorm, can be used for jobs
	DB      *gorm.DB
	dbLog   *DbLog
	gormLog logger.MultiLogger
)

func init() {
	revel.RegisterModuleInit(func(m *revel.Module) {
		gormLog = m.Log
	})
}

// OpenDB opens database connection
func OpenDB(dbDriver gorm.Dialector, gormConfig *gorm.Config, log bool) {
	db, err := gorm.Open(dbDriver, gormConfig)
	if err != nil {
		gormLog.Fatalf("SQL.Open failed with error %v", err)
	}
	DB = db
}

// DbInfo holds database information
type DbInfo struct {
	DbDriver   string
	DbHost     string
	DbPort     int
	DbUser     string
	DbPassword string
	DbName     string
	DbLog      bool
}

type DbLog struct {
}

func (dbl *DbLog) LogMode(gLvl glog.LogLevel) glog.Interface {
	return dbl
}
func (dbl *DbLog) Info(ctx context.Context, str string, params ...interface{}) {
	gormLog.Info(str)
}
func (dbl *DbLog) Warn(ctx context.Context, str string, params ...interface{}) {
	gormLog.Warn(str)
}
func (d *DbLog) Error(ctx context.Context, str string, params ...interface{}) {
	gormLog.Error(str)
}
func (dbl *DbLog) Trace(ctx context.Context, begin time.Time, fc func() (string, int64), err error) {
}

// InitDBWithParameters initializes database connection
func InitDBWithParameters(params DbInfo) {
	var dbDialector gorm.Dialector
	var dsn string
	switch params.DbDriver {
	case "postgres":
		dsn = fmt.Sprintf("host=%s port=%d user=%s dbname=%s sslmode=disable password=%s", params.DbHost, params.DbPort, params.DbUser, params.DbName, params.DbPassword)
		dbDialector = postgres.New(postgres.Config{DSN: dsn})
	case "mysql":
		dsn = fmt.Sprintf("%s:%s@(%s:%d)/%s?charset=utf8&parseTime=True&loc=Local", params.DbUser, params.DbPassword, params.DbHost, params.DbPort, params.DbName)
		dbDialector = mysql.New(mysql.Config{DSN: dsn})
	}
	OpenDB(dbDialector, &gorm.Config{Logger: dbLog}, params.DbLog)
	gormLog.Infof("Connected to %v", dsn)
}

// InitDB initializes database information
func InitDB() {
	params := DbInfo{}
	params.DbDriver = revel.Config.StringDefault("db.driver", "sqlite3")
	params.DbHost = revel.Config.StringDefault("db.host", "localhost")

	switch params.DbDriver {
	case "postgres":
		params.DbPort = revel.Config.IntDefault("db.port", 5432)
	case "mysql":
		params.DbPort = revel.Config.IntDefault("db.port", 3306)
	case "sqlite3":
		if params.DbHost == "localhost" {
			params.DbHost = "/tmp/app.db"
		}
	}

	params.DbUser = revel.Config.StringDefault("db.user", "default")
	params.DbPassword = revel.Config.StringDefault("db.password", "")
	params.DbName = revel.Config.StringDefault("db.name", "default")

	params.DbLog = revel.Config.BoolDefault("db.log", false)

	InitDBWithParameters(params)
}
