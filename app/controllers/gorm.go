package controllers

import (
	"github.com/revel/revel"
	"gorm.io/gorm"

	gormdb "gitlab.com/bate-modules/gorm/app"
)

// Gorm is a Revel controller with a pointer to the opened database
type Gorm struct {
	*revel.Controller
	DB *gorm.DB
}

// SetDB sets controllers db
func (c *Gorm) SetDB() revel.Result {
	c.DB = gormdb.DB
	return nil
}
