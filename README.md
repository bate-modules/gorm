# GORM
Implementation is a mixture of [security and performance fix](https://stackoverflow.com/a/47022061/1176156) and [official orm module](https://github.com/revel/modules/blob/master/orm/gorm/app/controllers/gorm.go).  
For docs see [GORM docs](https://gorm.io/docs/).

Implementation is a mixture of [security and performance fix](https://stackoverflow.com/a/47022061/1176156) and [official orm module](https://github.com/revel/modules/blob/master/orm/gorm/app/controllers/gorm.go).  
For docs see [GORM docs](https://gorm.io/docs/).

## Configuration
```ini
# Database config
# defaults:
# db.driver
#   "sqlite3"
# db.host
#   "localhost" (which resolves to /tmp/app.db when db.driver is sqlite3)
# db.port
#   ""
# db.user
#   default
# db.name
#   default
# db.password
#   ""
# db.singulartable  
#   false
# db.log
#   false
# db.autoinit
#   true
```